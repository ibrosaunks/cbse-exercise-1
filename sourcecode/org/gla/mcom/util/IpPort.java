package org.gla.mcom.util;

/**
 * The Class IpPort.
 * 
 * Contain ip and port.
 * Check port number format.
 */
public class IpPort {
	
	private String ip;
	
	private int port;

	public IpPort(String ip_port){
		String[] temp = ip_port.split(":");
		ip = temp[0];
		try {
			port = Integer.parseInt(temp[1]);
		} catch (NumberFormatException nfe) {
			// port is invalid
			port = -1;
		}
	}

	public String getIp() {
		return ip;
	}
	
	public int getPort() {
		return port;
	}
	
	public boolean isPortInValid(){
		return port == -1;
	}
	
	public String toString(){
		return ip +":" + port;
	}
	
	public static String convertIpPortToString(String ip, int port){
		return ip +":" + port;
	}
}
