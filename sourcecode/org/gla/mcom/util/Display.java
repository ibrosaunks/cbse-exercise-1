/**
 * Display: UI Console
 */
package org.gla.mcom.util;

import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import org.gla.mcom.impl.ReceiverImpl;
import org.gla.mcom.impl.RegistryImpl;
import org.gla.mcom.impl.SenderImpl;
import org.gla.mcom.init.Initialiser;

import jlibs.core.lang.Ansi;

public class Display {
	
	private static Map<String, String> commands = new LinkedHashMap<String, String>();
	public static final Ansi ansi_help = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.RED, null);
	public static final Ansi ansi_error = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.RED, null);
	public static final Ansi ansi_normal = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.RED, null);
	public static final Ansi ansi_header = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.BLUE, null);
	public static final Ansi ansi_normal2 = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.GREEN, null);
	public static final Ansi ansi_console = new Ansi(Ansi.Attribute.NORMAL, Ansi.Color.BLUE, null);
	
	private static SenderImpl sender;

	public Display(){
		System.out.println(" ");
    	System.out.println(ansi_header.colorize("________________TCPComponent__________________"));
    	System.out.println(ansi_header.colorize("     Component message passing via sockets    "));
    	System.out.println(ansi_normal2.colorize("running on:"+Initialiser.local_address.getHostAddress()));
    	System.out.println(ansi_normal2.colorize("Listening port:"+ReceiverImpl.listenSocket.getLocalPort()));
    	System.out.println(ansi_header.colorize("______________________________________________"));
    	System.out.println(ansi_normal.colorize("Type ? for help                               "));
    	    	
    	initCommands();
    	groupRegister();
       	console();  
	}
	
	private static void initCommands(){//
		commands.put("ipr% <ipr>", "set ip address<hip> of message recipient");
		commands.put("p% <p>", "set listening port<p> of message recipient");
		commands.put("<m>", "send message <m> to recipient");
		
		commands.put("rr% <registry_operation>", "choose whether to advertise or deregister (reg, dereg)");
		commands.put("c% <c>", "check if host with name <c> exist");
		
		commands.put("lookup",  "does a lookup for available recipients in all the available registrars");
		commands.put("crs", "cease Registrar service");
		commands.put("srs", "start Regitrar service");
		
		commands.put("end", "terminate");	
	}
	
	@SuppressWarnings("resource")
	private static void console(){
    	System.out.println(ansi_console.colorize  ("\ncom("+Initialiser.local_address.getHostAddress()+")>>"));

		Scanner scanner = new Scanner(System.in);
		String command = scanner.nextLine();
		
		execute(command);
	}

	private static void execute(String command){
		if(command.equals("?")){
			printCommands();
		}
		else if(command.contains("%")){
			String[] c = command.split("%");
			if(c.length <2 ){
		    	System.out.println(ansi_error.colorize("ERROR:Invalid format"));
			}
			else{
				String operation = c[0];
				String value = c[1];
				
				if(operation.equals("ipr")){
					Initialiser.receiver_ip = value;
				}
				else if(operation.equals("p")){
					if(Initialiser.receiver_ip == null){
				    	System.out.println(ansi_error.colorize("ERROR:receiver not set"));
					}
					else{
						if(isNumeric(value)){
							Initialiser.receiver_listening_port = new Integer(value).intValue();
							sender = new SenderImpl();
							boolean connected = sender.makeConnection();							
							if(!connected){
						    	System.out.println(ansi_error.colorize("ERROR:connection failed"));
							}	
						}
						else{
					    	System.out.println(ansi_error.colorize("ERROR:host port NaN"));
						}
					}
				}
				else if(operation.equals("c")){
					InetAddress address = IPResolver.getAddress(value);
					if(address != null){
						System.out.println(ansi_normal2.colorize("check passed"));
						System.out.println(ansi_normal2.colorize("running on:"+address.getHostAddress()));
				    	System.out.println(ansi_normal2.colorize("DNS:"+address.getHostName()));
					}
					else{
						System.out.println(ansi_normal2.colorize("check failed"));
					}
				}
				else if(operation.equals("rr")){
					if(value.equalsIgnoreCase("reg") || value.equalsIgnoreCase("dereg") ){
						sender.sendMessage(value + ":" + IpPort.convertIpPortToString(Initialiser.local_address.getHostAddress(),ReceiverImpl.listenSocket.getLocalPort()), Initialiser.receiver_ip, Initialiser.receiver_listening_port );
						
						if(value.equalsIgnoreCase("reg"))
							Initialiser.availableRegistrars.add(IpPort.convertIpPortToString(Initialiser.receiver_ip,Initialiser.receiver_listening_port));
						else if(value.equalsIgnoreCase("dereg")){
							if(Initialiser.availableRegistrars.contains(IpPort.convertIpPortToString(Initialiser.receiver_ip,Initialiser.receiver_listening_port)))
								Initialiser.availableRegistrars.remove(IpPort.convertIpPortToString(Initialiser.receiver_ip,Initialiser.receiver_listening_port));
						}
							
					}
					else
						System.out.println(ansi_error.colorize("ERROR:invalid operation"));
				} 
			}
		}
		//Do a lookup
		else if (command.equals("lookup")){
			sender.lookupMessage();
		}
		//Cease registrar service
		else if(command.equals("crs")){
			if (Initialiser.regImpl == null) {
				System.out.println(ansi_normal.colorize("Registrar service has already stopped"));
			} else {
				CeaseRegistrar();
			}
			
		}
		//Start registrar service
		else if(command.equals("srs")){
			if (Initialiser.regImpl != null) {
				System.out.println(ansi_normal.colorize("Registrar service has already started"));
			} else {
				Initialiser.regImpl = new RegistryImpl();
			}
		}
		else if(command.equals("end")){
			if (Initialiser.regImpl != null) CeaseRegistrar();
			if(Initialiser.availableRegistrars != null && !Initialiser.availableRegistrars.isEmpty() ) groupDeregister();
			System.exit(0);
		}
		else if(command.length() > 0){
			sender.sendMessage(command);			
		}
		console();
	}
	
	@SuppressWarnings("rawtypes")
	private static void printCommands(){
		Iterator it = commands.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        ansi_help.out("["+pairs.getKey() + "]: ");
	        System.out.println(pairs.getValue());
	    }
	}
	
	private static boolean isNumeric(String str) {
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	private static void CeaseRegistrar(){
		sender = new SenderImpl();
		String[] ip_ports = Initialiser.regImpl.lookup();
		if(ip_ports != null && ip_ports.length != 0){
			for(String ip_port : ip_ports){
				IpPort ipport = new IpPort(ip_port);
				if (ipport.isPortInValid()) {
					// port is not integer
					System.out.println(ansi_error.colorize("ERROR: "+ipport.getIp()+" port NaN"));
					continue;
				}
				
				sender.sendMessage(IpPort.convertIpPortToString(Initialiser.local_address.getHostAddress(),ReceiverImpl.listenSocket.getLocalPort()) + " ending Registrar service", 
						ipport.getIp(), ipport.getPort());
			}
		}
		Initialiser.regImpl = null;
	}
	
	private static void groupDeregister(){
		for(String ip_port : Initialiser.availableRegistrars){
			IpPort ipport = new IpPort(ip_port);
			if (ipport.isPortInValid()) {
				// port is not integer
				System.out.println(ansi_error.colorize("ERROR: "+ipport.getIp()+" port NaN"));
				continue;
			}
			
			sender.sendMessage( "dereg:" + IpPort.convertIpPortToString(Initialiser.local_address.getHostAddress(),ReceiverImpl.listenSocket.getLocalPort()), 
					ipport.getIp(), ipport.getPort());
		}
	}
	
	private void groupRegister() {
		sender = new SenderImpl();	
		for(String ip_port: Initialiser.availableRegistrars){
			IpPort ipport = new IpPort(ip_port);
			if (ipport.isPortInValid()) {
				// port is not integer
				System.out.println(ansi_error.colorize("ERROR: "+ipport.getIp()+" port NaN"));
				continue;
			}
			
			sender.sendMessage("reg:"+  IpPort.convertIpPortToString(Initialiser.local_address.getHostAddress(),ReceiverImpl.listenSocket.getLocalPort()), ipport.getIp(), ipport.getPort() );
			
		}
	}
}
