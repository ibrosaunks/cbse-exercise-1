/**
 * The Sender makes connection request and send messages to a connected recipient
 */

package org.gla.mcom.impl;

import java.net.*;
import java.util.HashSet;
import java.io.*;

import org.gla.mcom.Sender;
import org.gla.mcom.init.Initialiser;
import org.gla.mcom.util.Display;
import org.gla.mcom.util.IpPort;

public class SenderImpl implements Sender{
	
	private boolean accepted;
	public boolean makeConnection() {
		
		try {
			System.out.println("initiating tcp connection to "+Initialiser.receiver_ip+":"+Initialiser.receiver_listening_port+")");
			String i_message = "ping";
			Socket serverSocket = new Socket(Initialiser.receiver_ip,Initialiser.receiver_listening_port);
			DataInputStream in = new DataInputStream(serverSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(serverSocket.getOutputStream());
			out.writeUTF(i_message); // UTF is a string encoding;
			
			String r_message = in.readUTF();
			if(i_message != null){
				if(r_message.equals("rejected")){
					accepted = false;
				}
				else if(r_message.equals("accepted")){
					accepted = true;
				}
				System.out.println(Display.ansi_normal2.colorize("connection to "+IpPort.convertIpPortToString(Initialiser.receiver_ip,Initialiser.receiver_listening_port)+" "+r_message));
				
			}			
			serverSocket.close();
		} 
		catch (UnknownHostException e) {
			System.out.println("Sock:" + e.getMessage());
		} 
		catch (EOFException e) {
			System.out.println("EOF:" + e.getMessage());
		} 
		catch (IOException e) {
			System.out.println("IO:" + e.getMessage());
		}
		
		return accepted;
	}
	
	public void sendMessage(String message) {
		if(accepted){
			
			try {
				Socket serverSocket = new Socket(Initialiser.receiver_ip,Initialiser.receiver_listening_port);
				DataOutputStream out = new DataOutputStream(serverSocket.getOutputStream());
				out.writeUTF(message); // UTF is a string encoding;
				serverSocket.close();
			} 
			catch (UnknownHostException e) {
				System.out.println("Sock:" + e.getMessage());
			} 
			catch (EOFException e) {
				System.out.println("EOF:" + e.getMessage());
			} 
			catch (IOException e) {
				System.out.println("IO:" + e.getMessage());
			}			
		}
		else{
			System.out.println(Display.ansi_error.colorize("ERROR:No message recipient"));
		}		
	}
	
	public void lookupMessage(){
		if (Initialiser.availableRegistrars.isEmpty()){
			System.out.println("There are no available registrars");
			return;
		}
		try {
			HashSet<String> returnedInstances = new HashSet<String>();
			for (String ip_port:Initialiser.availableRegistrars) {
				IpPort ipport = new IpPort(ip_port);
				if (ipport.isPortInValid()) {
					// port is not number
					continue;
				}
				Socket serverSocket = new Socket(ipport.getIp(), ipport.getPort());
				DataOutputStream out = new DataOutputStream(serverSocket.getOutputStream());
				DataInputStream in = new DataInputStream(serverSocket.getInputStream());
				out.writeUTF("lookup"); // UTF is a string encoding;
				String instances = in.readUTF();
				for (String instance: instances.split("\n")){
					returnedInstances.add(instance);
				}
				serverSocket.close();
			}
			if (returnedInstances.isEmpty()){
				System.out.println("No available instances were found in the registrars");
				return;
			}
			for (String reg: returnedInstances){
				System.out.println(reg);
			}
		} 
		catch (UnknownHostException e) {
			System.out.println("Sock:" + e.getMessage());
		} 
		catch (EOFException e) {
			System.out.println("EOF:" + e.getMessage());
		} 
		catch (IOException e) {
			System.out.println("IO:" + e.getMessage());
		}			
	}
	
	public void sendMessage(String message, String receiver_ip, int receiver_listening_port){
		
			try {
				
				Socket serverSocket = new Socket(receiver_ip, receiver_listening_port);
				DataOutputStream out = new DataOutputStream(serverSocket.getOutputStream());
				
				DataInputStream in = new DataInputStream(serverSocket.getInputStream());
				
				out.writeUTF(message); // UTF is a string encoding;
				if(message.contains("reg:") || message.contains("dereg:"))
					
					System.out.println(in.readUTF());
				serverSocket.close();
			} 
			catch (UnknownHostException e) {
				System.out.println("Sock:" + e.getMessage());
			} 
			catch (EOFException e) {
				System.out.println("EOF:" + e.getMessage());
			} 
			catch (IOException e) {
				System.out.println("IO:" + e.getMessage());
			}			
		}
			
	
}