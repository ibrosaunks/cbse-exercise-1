package org.gla.mcom.impl;

import java.util.ArrayList;
import java.util.List;

import org.gla.mcom.Registry;

public class RegistryImpl implements Registry{

	public static List<String> recipients = new ArrayList< String>();
	
	@Override
	public String[] lookup() {
		String[] arr = new String[recipients.size()];
		
		return  recipients.toArray(arr);
	}

	@Override
	public boolean register(String ip_port) {
		boolean registered = false;
		if(!recipients.contains(ip_port)){
			registered = recipients.add(ip_port);
		}
		System.out.println("Adding " + ip_port + " to the registry");
		return registered;
	}

	@Override
	public boolean deregister(String ip_port) {
		System.out.println("Removing " + ip_port + " to the registry");
		return recipients.remove(ip_port);
	}

	
}
