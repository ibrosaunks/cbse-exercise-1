/**
 * mComponent Initialiser:
 * (1) initialise local host address
 * (2) start a Receiver to listen to incoming messages
 * (3) creates a RegistryImpl instance which helps the instance act as a registrar
 * (4) creates a display for console interaction
 */

package org.gla.mcom.init;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.gla.mcom.impl.ReceiverImpl;
import org.gla.mcom.impl.RegistryImpl;
import org.gla.mcom.impl.SenderImpl;
import org.gla.mcom.util.Display;
import org.gla.mcom.util.IPResolver;
import org.gla.mcom.util.IpPort;

public class Initialiser {

	public static String receiver_ip;
	public static int receiver_listening_port = -1;

	public static InetAddress local_address = null;
	public static ReceiverImpl receiver;
	public static List<String> availableRegistrars = null;
	public static RegistryImpl regImpl;

	public static void main(String args[]) {

		regImpl = new RegistryImpl();
		availableRegistrars = new ArrayList<String>();

		try {
			local_address = IPResolver.getLocalHostLANAddress();
			startReceiver();
			if (args.length > 0) {
				SenderImpl sender;
				for (String ip_port : args) {
					IpPort ipport = new IpPort(ip_port);
					if (ipport.isPortInValid()) {
						// port is not an integer
						continue;
					}
					receiver_ip = ipport.getIp();
					receiver_listening_port = ipport.getPort();
					sender = new SenderImpl();
					boolean connected = sender.makeConnection();
					if (connected) {
						availableRegistrars.add(ip_port);
					}
				}
				// reset receiver ip and port
				receiver_ip = null;
				receiver_listening_port = -1;
			}
			new Display();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private static void startReceiver() {
		receiver = new ReceiverImpl();
		receiver.receiveMessage();
	}

}
